# Datenimport

## Häufige Dateiformate
## Importdialog in RStudio
## Importieren über Skript !!!
## Prinzipien: Daten verstehen; Import kontrollieren, dokumentieren, skalieren
## Codebook, Fragebogen  !!!!

# ---

## DATEN VERSTEHEN

## Header
## Delimiter / Separator
## Dezimaltrennzeichen
## Fehlende Werte
## Strings
## Encoding


# ---

## Base R: read.csv(), read.csv2(); read.table()


## Beispiel ESS

## TIDYVERSE: readr: "parse a flat file into a tibble"

## CSV-Datei -> Matrix -> Spaltentypen raten -> Tibble

# ---

## Skript für den Datenimport anlegen!

## Arbeitsverzeichnis definieren

getwd()
setwd("~/ownCloud/Lehre/R Essentials/r-essentials-4/") # Absoluter Pfad
getwd()
setwd("data") # Relative Pfade funktionieren auch ...
getwd()

## Benötigte Pakete installieren / laden
library(tidyverse)
library(haven)
if (!("labelled" %in% installed.packages())) install.packages("labelled")
library(labelled)




guess_encoding("data/ESS1-8e01.csv")
# utf-8
# iscoXXX

read_csv(file = "ESS1-8e01.csv")


### Spaltentypen nicht gut, inklusive "dbl" statt "int"
spec_csv("ESS1-8e01.csv")


### hinctnt ?! Alles fehlende Werte (na_logical)

ess_csv <- read_csv(file = "ESS1-8e01.csv")

str(ess_csv)

View(ess_csv)

### Plus: einige für unsere Zwecke unnötige Spalten

ess_csv <- read_csv(file = "data/ESS1-8e01.csv",
                    col_types = cols(
                      cntry = col_skip(),
                      cname = col_skip(),
                      cedition = col_skip(),
                      cproddat = col_skip(),
                      cseqno = col_skip(),
                      name = col_skip(),
                      essround = col_skip(),
                      edition = col_skip(),
                      idno = col_integer(),
                      dweight = col_skip(),
                      pspwght = col_skip(),
                      pweight = col_skip(),
                      polintr = col_integer(),
                      trstplc = col_integer(),
                      trstprt = col_integer(),
                      vote = col_integer(),
                      imsmetn = col_integer(),
                      imdfetn = col_integer(),
                      impcntr = col_integer(),
                      chldhhe = col_integer(),
                      edulvla = col_integer(),
                      uemp3m = col_integer(),
                      hinctnt = col_double()
                    )
)

View(ess_csv)

colnames(ess_csv)
rownames(ess_csv)


ess_csv <- read_csv(file = "ESS1-8e01.csv",
                    col_types = cols_only(
                      idno = col_integer(),
                      polintr = col_integer(),
                      trstplc = col_integer(),
                      trstprt = col_integer(),
                      vote = col_integer(),   # logical wäre hier auch möglich, wäre der Wertebereich anders ...
                      imsmetn = col_integer(),
                      imdfetn = col_integer(),
                      impcntr = col_integer(),
                      chldhhe = col_integer(),
                      edulvla = col_integer(),
                      uemp3m = col_integer(),
                      hinctnt = col_double()
                    )
)

str(ess_csv)

attributes(ess_csv)

?cols_only

ess_csv <- read_csv(file = "ESS1-8e01.csv",
                    col_types = cols_only(
                      idno = "i",
                      polintr = "i",
                      trstplc = "i",
                      trstprt = "i",
                      vote = "i",
                      imsmetn = "i",
                      imdfetn = "i",
                      impcntr = "i",
                      chldhhe = "i",
                      edulvla = "i",
                      uemp3m = "i",
                      hinctnt = "d"
                    )
)


ess_csv <- read_csv(file = "ESS1-8e01.csv",
                    col_types = "--------i---iiiiiiiiiid"   # Entspricht eigentlich nicht der Tidyverse-Etikette
                    )
glimpse(ess_csv)
str(ess_csv)

### Nächster Schritt: Kontrolle und weitere Spezifikationen
summary(ess_csv)

with(ess_csv,
     table(edulvla)) # ! 55, 77, 88
with(ess_csv,
     table(uemp3m)) # ! 7
with(ess_csv,
     table(polintr)) # ! 8

ess_csv <- read_csv(file = "data/ESS1-8e01.csv",
                    col_types = "--------i---iiiiiiiiiid",
                    na = c("", " ", "55", "77", "88", "NA", "Missing")   # Vorsicht: global definiert (für alle Spalten)
)

View(ess_csv)

summary(ess_csv)


### Exportieren und Speichern

write_csv(ess_csv, "ess_csv_export.csv")

write_excel_csv()

write_rds(ess_csv, "ess_csv.rds")


## Spaltennamen

ess_csv_2 <- read_csv("ess_csv_export.csv")

ess_csv_2 <- read_csv("ess_csv_export.csv",    # In diesem Datensatz sind nur mehr die gewünschten Variablen enthalten
                      col_types = "iiiiiiiiiiid",
                      col_names = c(
                        "ID",
                        "Pol_Int",
                        "Trust_Pol",
                        "Trust_Part",
                        "Voted",
                        "Allow_Im_Same",
                        "Allow_Im_Diff",
                        "Immig_Cntr",
                        "Child_Hh",
                        "Educ_lvl",
                        "Unempl_3m",
                        "Hh_Income"
                      ))


ess_csv_2


ess_csv_2 <- read_csv("ess_csv_export.csv",
                      col_types = cols(
                        "ID" = col_integer(),
                        "Pol_Int" = col_integer(),
                        "Trust_Pol" = col_integer(),
                        "Trust_Part" = col_integer(),
                        "Voted" = col_integer(),
                        "Allow_Im_Same" = col_integer(),
                        "Allow_Im_Diff" = col_integer(),
                        "Immig_Cntr" = col_integer(),
                        "Child_Hh" = col_integer(),
                        "Educ_lvl" = col_integer(),
                        "Unempl_3m" = col_integer(),
                        "Hh_Income" = col_double()
                      ),
                      col_names = c(
                        "ID",
                        "Pol_Int",
                        "Trust_Pol",
                        "Trust_Part",
                        "Voted",
                        "Allow_Im_Same",
                        "Allow_Im_Diff",
                        "Immig_Cntr",
                        "Child_Hh",
                        "Educ_lvl",
                        "Unempl_3m",
                        "Hh_Income"
                      ),
                      )

problems(ess_csv_2)

ess_csv_2 <- read_csv("ess_csv_export.csv",
                      col_names = c(
                        "ID",
                        "Pol_Int",
                        "Trust_Pol",
                        "Trust_Part",
                        "Voted",
                        "Allow_Im_Same",
                        "Allow_Im_Diff",
                        "Immig_Cntr",
                        "Child_Hh",
                        "Educ_lvl",
                        "Unempl_3m",
                        "Hh_Income"
                      ),
                      col_types = cols(
                        "ID" = col_integer(),
                        "Pol_Int" = col_integer(),
                        "Trust_Pol" = col_integer(),
                        "Trust_Part" = col_integer(),
                        "Voted" = col_integer(),
                        "Allow_Im_Same" = col_integer(),
                        "Allow_Im_Diff" = col_integer(),
                        "Immig_Cntr" = col_integer(),
                        "Child_Hh" = col_integer(),
                        "Educ_lvl" = col_integer(),
                        "Unempl_3m" = col_integer(),
                        "Hh_Income" = col_double()
                      ),
                      skip = 1
)

problems(ess_csv_2)

## Besser: Schritt für Schritt, keine undurchsichtigen Workarounds ...

ess_csv_2

### Andere readr-Pakete, die in der Praxis relevant sein können
read_csv2()
read_table()
read_delim()
read_tsv()
read_csv2

### SPSS, haven, labelled

library(foreign)
?read.spss()
?read_spss()

library(haven)  # haven ist Teil des Tidyverse, aber keines seiner Kernpakete.
library(labelled)  # labelled ist eine sinnvolle Ergänzung zu haven

read_spss("ESS1-8e01.sav")

ess_spss <- read_spss("ESS1-8e01.sav")

class(ess_spss$polintr)

var_label(ess_spss)$vote

look_for(ess_spss)

look_for(ess_spss, "polintr")

glimpse(look_for(ess_spss, "polintr"))

look_for(ess_spss, "polintr")$value_labels

val_labels(ess_spss)
val_label(ess_spss$polintr, 2)
val_label(ess_spss$polintr, 2) <- "Somewhat interested"
look_for(ess_spss, "polintr")

look_for(ess_spss, "edulvla")

table(ess_spss$edulvla)

### Fehlende Werte als eigenes Kapitel - Kontrolle bewahren!

na_values(ess_spss)

ess_spss[is.na(ess_spss$edulvla), 15:23]

na_values(ess_spss$edulvla)

na_values(ess_spss$edulvla) <- 55

na_values(ess_spss$edulvla)

ess_spss[is.na(ess_spss$edulvla), 15:23]

table(ess_spss$edulvla)

ess_spss_na <- user_na_to_na(ess_spss)

table(ess_spss_na$edulvla)

summary(ess_spss$edulvla)

summary(ess_spss_na$edulvla)


write_sav(ess_spss, "ess_sav_exported.sav")




