# Hier sind meine Helferfunktionen

# Fancy Quotes --------------------------------------------------------------------------------

fancy_quotes <- function(pipe_object) {
    paste0("«", pipe_object, "»")
}

# Labels --------------------------------------------------------------------------------------

convert_labels_if_contains <- function (tribble, string) {
    mutate(
        tribble,
        across(contains(string), as_factor),
        n = n()
    )
}
