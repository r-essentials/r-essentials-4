---
title: "Das Tidyverse"
subtitle: "R::Essentials_4"
author: "Ken Horvath & Fabian Mundt"
date: 25.11.2020 -- 26.11.2020
output: 
    tufte::tufte_html:
        highlight: pygments
        tufte_variant: envisioned
editor_options: 
  chunk_output_type: console
---

```{r setup, echo=FALSE, warning=FALSE, message=FALSE}
# Pakete
library(tufte)

# Globale knitr Optionen
knitr::opts_chunk$set(comment = "#>")
```

## Tidyverse == R packages for data science

```{marginfigure}
https://www.tidyverse.org
```

> »The tidyverse is an opinionated collection of R packages designed for data science. All packages share an underlying design philosophy, grammar, and data structures. […] The tidyverse is a language for solving data science challenges with R code.« 
>
>`r quote_footer("--- Hadley Wickham")`

## Prinzipien 

```{marginfigure}
https://tidyverse.tidyverse.org/articles/manifesto.html
```

1. Reuse existing data structures.
2. Compose simple functions with the pipe.
3. Embrace functional programming.
4. Design for humans.

## Pakete

```{r}
# Das tidyverse-Paket ist ein framework Paket, das lediglich
# alle tidyverse-Pakete lädt oder installiert.
library("tidyverse")
```

## Data science workflow

```{marginfigure}
© by Joseph Rickert
```

![](img/tidyverse.png)

## Die Grundlagen: Datenstrukturen

:::: fullwidth
![](https://media.giphy.com/media/hrLkskKw6dmhTQceq0/giphy.gif){width=100%}
::::

```{marginfigure}
Die Inhalte orientieren sich an _Advanced R_ von Hadley Wickham. Die Lektüre bietet sich ideal 
als Vertiefung an: https://adv-r.hadley.nz
```

In R existieren _fünf_ Basisstrukturen, in denen Daten organisiert werden können. Sie lassen sich anhand ihrer Dimensionalität und Homogenität (die Struktur besteht aus einer oder mehrerer Datentypen) unterscheiden.

```{marginfigure}
Das _tidyverse_ führt mit `tibble()` eine moderne Form von data frames ein.
```

|    | Homogen       | Heterogen  |
|----|---------------|------------|
| 1d | Atomic vector | List       |
| 2d | Matrix        | Data frame |
| nd | Array         |            |

_Alle Objekte in R basieren auf diesen fünf Datentypen._  

## Data frames

Data frames dürften die am meisten verwendete Struktur in R zum Speichern von Daten sein. Insofern ist es kein Wunder, dass data frames das größte Potenzial dafür haben, den eigenen Code eleganter und effizienter zu machen. Wendet man data frames systematisch und sinnvoll an, tragen sie zu einer wesentlichen Verbesserung der R Anwendung bei. Insbesondere die moderne Variante `tibble()` räumt viele überkommene Stolpersteine aus dem Weg. Im weiteren Verlauf werden wir häufig mit tibbles arbeiten, weswegen dieser Abschnitt etwas ausführlicher ist.

### Erzeugen

```{r}
df_seen_schweiz <- data.frame(
    name = c("Genfersee", "Neuenburgersee", "Bodensee", "Vierwaldstättersee"),
    flaeche_schweiz = c(345.31, 215.20, 172.86, 113.72),
    tiefe = c(310, 153, 254, 214)
)

df_seen_schweiz

str(df_seen_schweiz)
```

Aus historischen Gründen wurden bis R 4.0.0 beim Erzeugen eines Data frames alle Strings in Faktoren konvertiert. Heutzutage ist der intendierte Leistungsgewinn nicht mehr relevant. Mehr noch, das Verhalten ist häufig die Ursache für spätere Fehler. Aus diesem Grund wurde die Option `stringAsFactors = FALSE` ab R 4.0.0 zum Standard.

```{r}
typeof(df_seen_schweiz)

class(df_seen_schweiz)

is.data.frame(df_seen_schweiz)
```

Mit `as.data.frame()` können aus anderen Objekten data frames erzeugt werden.

### Kombinieren

Data frames können spalten- und/oder zeilenweise erweitert werden. Dabei müssen die Dimensionen des data frames beachtet werden. Ferner ist es nicht möglich, Vektoren mit `cbind()` zu einem Data frame zu kombinieren. Hierfür sollte direkt `data.frame()` verwendet werden.

```{r}
cbind(df_seen_schweiz, data.frame(volumen = c(89, 14, 48, 11.9)))

rbind(df_seen_schweiz, data.frame(name = "Zürichsee", flaeche_schweiz = 88.17, tiefe = 136))

df_seen_schweiz
```

## Tibbles (tidyverse)

:::: fullwidth
![](https://media.giphy.com/media/eNjn0dffKMVzWJJrXH/giphy.gif){width=100%}
::::

Tibbles sind die grundlegende Datenstruktur des _tidyverse_. Es ist eine moderne Implementierung von data frames. Dabei wurde alles frustrierende und fehlerinduzierende beseitigt und die positiven Aspekte der wichtigen Datenstruktur verbessert.

### Erzeugen

```{r}
# Statt data.frame wird einfach die Funktion tibble aufgerufen.
df <- tibble(1, "hello", "lucerne", TRUE)
str(df)
is.data.frame(df)
is.list(df)
is.atomic(df)
is_tibble(df)

# Spezielle Tabellen sind problemlos möglich.
df_seen_sw_tibble <- tibble(
    name = c("Genfersee", "Neuenburgersee", "Bodensee", "Vierwaldstättersee"),
    flaeche_schweiz = c(345.31, 215.20, 172.86, 113.72),
    tiefe = c(310, 153, 254, 214)
)
# Die Anzeige in der R-Konsole wurde deutlich verbessert.
```

### Konvertieren

```{r}
df_seen_schweiz
class(df_seen_schweiz)

# There and
df_seen_schweiz <- 
  df_seen_schweiz %>% 
  as_tibble() %>% 
  as.data.frame()

df_seen_schweiz <- as_tibble(as.data.frame(df_seen_schweiz))

df_seen_schweiz
class(df_seen_schweiz)

# Back again
df_seen_schweiz <- as.data.frame(df_seen_schweiz)
class(df_seen_schweiz)
df_seen_schweiz
```

### Ausgabe

```{r}
# Base R
bad <- data.frame(1:300, rep(letters, 300))
bad
# Tidyverse
good <- tibble(1:300, rep("abc", 300))
good
# Kontrolle der Ausgabelänge
tibble(1:300, rep("abc", 300)) %>% print(n=53)
```

## `tibble()` vs. `data.frame()`

`tibble()` verändert nie den Typ der Eingabeobjekte. `stringFactors = FALSE` wird nicht, 
wie es vor R 4.0.0 der Fall war, 
benötigt:

```{r}
tibble(x = c("a", "b", "c", "d"))

tibble(x = factor(c("a", "b", "c", "d")))
```

`tibble()` verändert niemals die Namen von Variablen:

```{r}
names(data.frame(`crazy name` = 1))
#> [1] "crazy.name"

names(tibble(`crazy name` = 1))
#> [1] "crazy name"
```

`tibble()` erzeugt niemals Zeilennamen. Warum? "The whole point of tidy data is to store variables in a consistent way. So it never stores a variable as special attribute." Damit soll also verhindert werden, dass Informationen in einer "künstlichen" Spalte gespeichert werden. Alle relevanten Daten sollen in den Zeilen und Spalten des data frames vorliegen.

```{r}
df <- data.frame(x = c("a", "b", "c"), numbers = 1:3, row.names = 1)
df

as_tibble(df)
```

`tibble()` akzeptiert ausschließlich atomare Vektoren der Länge 1. Warum? Die Kombination mehrdimensionaler Objekte ist eine große Fehlerquelle.

```{r}
tibble(data.frame("x", "y", "z"), TRUE)
#> Error: Column `data.frame("x", "y", "z")` must be a 1d atomic vector or a list
```

Das Subsetting eines `tibble()` wird immer zu einem `tibble()` führen. `drop = FALSE` wird nicht benötigt.

```{r}
df1 <- data.frame(x = 1:3, y = 3:1)
class(df1[, 1:2])
#> [1] "data.frame"
class(df1[, 1])
#> [1] "integer"

df2 <- tibble(x = 1:3, y = 3:1)
class(df2[, 1:2])
#> [1] "tbl_df"     "tbl"        "data.frame"
class(df2[, 1])
#> [1] "tbl_df"     "tbl"        "data.frame"
```
