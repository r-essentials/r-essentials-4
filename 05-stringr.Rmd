---
title: "Mit Strings arbeiten"
subtitle: "R::Essentials_4"
author: "Ken Horvath & Fabian Mundt"
date: 25.11.2020 -- 26.11.2020
output: 
    tufte::tufte_html:
        highlight: pygments
        tufte_variant: envisioned
editor_options: 
  chunk_output_type: console
---

```{r setup, echo=FALSE, warning=FALSE, message=FALSE}
# Pakete
library(tidyverse)
library(tufte)

# Globale knitr Optionen
knitr::opts_chunk$set(comment = "#>")
```

## Strings, die unterschätzen Arbeit mit Buchstabenfolgen

```{marginfigure}
https://stringr.tidyverse.org
```
